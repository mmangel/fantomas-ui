module Client

open Elmish
open Elmish.React

open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Shared
open Fulma
open Fable.FontAwesome
open Fable.FontAwesome.Free
open Fulma.Extensions.Wikiki
open ReactEditor
open Thoth.Json

open FSharp.Reflection

importSideEffects "./sass/style.sass"
module Result =
    let toOption = function
        | Ok x -> Some x
        | Error _ -> None

module URI =
    type QueryDataType = Encoded | Plain
    
    let private compressToEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private decompressFromEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private getURIhash(): string  = importMember "./js/util.js"
    let private setURIhash(_x: string): unit  = importMember "./js/util.js"

    let parseQuery queryTypes =
        getURIhash().Split[|'&'|]
        |> Seq.choose (fun s ->
            match s.Split[|'='|] |> Seq.toList with
            | [key; value] -> 
                let typ = queryTypes |> Map.tryFind key |> Option.defaultValue Encoded
                Some (key, match typ with Encoded -> decompressFromEncodedURIComponent value | Plain -> value)
            | _ -> None)
        |> Map.ofSeq

    let getQuery queryTypes xs =
        xs |> Seq.map (fun (key, value) -> 
            let typ = queryTypes |> Map.tryFind key |> Option.defaultValue Encoded
            key + "=" + match typ with Encoded -> compressToEncodedURIComponent value | Plain -> value)
         |> String.concat "&"            
    let updateQuery queryTypes xs =
        getQuery queryTypes xs |> setURIhash

// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
| VersionFound of string
| SetSourceText of string
| DoFormat of FormatConfig
| Formatted of string
| Error of string
| UpdateConfig of FormatConfig
| ChangeBackend of string

module Server =

    open Shared
    open Fable.Remoting.Client

    /// A proxy you can use to talk to server directly
    let api backendName : IModelApi =
      let backendUrl = Config.getBackendUrl backendName
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      #if !DEBUG
      |> Remoting.withBaseUrl backendUrl
      #endif
      |> Remoting.buildProxy<IModelApi>

let reInitCmd model =
    let cmd = Cmd.ofAsync (Server.api model.BackendName).version () VersionFound (fun ex -> Error ex.Message)
    Cmd.batch [cmd; Cmd.ofMsg (DoFormat model.Config)]

let queryTypes = Map.ofSeq ["fantomas", URI.Plain; "code", URI.Encoded; "config", URI.Encoded]

// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let initialModel = Model.Default
    let query = URI.parseQuery queryTypes
    let code = query |> Map.tryFind "code" |> Option.defaultValue ""
    let config =
        query |> Map.tryFind "config" |> Option.bind (Decode.Auto.fromString >> Result.toOption)
        |> Option.defaultValue initialModel.Config
    let backendName = (query |> Map.tryFind "fantomas" |> Option.defaultValue initialModel.BackendName)
    let m = { initialModel with Source = code; Config = config; BackendName = backendName }
    m, reInitCmd m

let updateQuery model = 
    let queryData =
        ["fantomas", (fun m -> m.BackendName); "code", (fun m -> m.Source); "config", (fun m -> Encode.Auto.toString(0, m.Config))]
        |> List.filter (fun ( _, selector) -> selector model <> selector Model.Default)
        |> List.map (fun (k, selector) -> k, selector model)
    URI.updateQuery queryTypes queryData

// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
    match msg with
    | SetSourceText x ->
        let nextModel = { currentModel with Source = x; FSharpEditorState = Loaded "" }
        nextModel, Cmd.none
    | Formatted x ->
        let nextModel = { currentModel with FSharpEditorState = Loaded x }
        nextModel, Cmd.none
    | Error x ->
        let nextModel = { currentModel with FSharpEditorState = FormatError x }
        nextModel, Cmd.none
    | DoFormat config ->
        updateQuery currentModel
        let response =
            Cmd.ofAsync
                (fun x -> (Server.api currentModel.BackendName).format x config)
                currentModel.Source
                (function | Ok x -> Formatted x | Result.Error e -> Error e)
                (fun e -> Error e.Message)
        { currentModel with FSharpEditorState = Loading }, response
    | UpdateConfig c -> { currentModel with Config = c }, Cmd.none

    | VersionFound version -> { currentModel with Version = version }, Cmd.none
    | ChangeBackend name -> 
        let m = { currentModel with BackendName = name; Version = Model.Default.Version }
        updateQuery m
        m, reInitCmd m

let options (config: FormatConfig) dispatch =
    let names = FSharpType.GetRecordFields (config.GetType()) |> Seq.map (fun x -> x.Name)
    let values = FSharpValue.GetRecordFields config
    let data = Seq.zip names values |> Seq.toArray
    let update fieldName x =
        let data = data |> Array.map (fun (k,v) -> if k=fieldName then k,x else k,v)
        FSharpValue.MakeRecord(config.GetType(), Array.map snd data) :?> FormatConfig

    let wrapColumn x =
        Column.column [Column.Width(Screen.All, Column.IsHalf)] (List.map snd x)

    data
    |> Seq.map (fun (k,v) ->
        match v with
        | :? int as v ->
            Field.div [ ] [ Label.label [ ] [ str k ]; Control.div [] [ Input.number [
                Input.Option.Value (string v); Input.Option.OnChange (fun ev -> dispatch (UpdateConfig <| update k (!!ev.target?value)))] ] ]
        | :? bool as v ->
            Field.div [ ] [ Control.div [] [ Checkbox.checkbox [ ]  [
                Switch.switch [ Switch.Id k; Switch.Checked v; Switch.OnChange (fun ev -> dispatch (UpdateConfig <| update k (!!ev.target?``checked``)))  ] [ str k ] ] ] ]
        | _ -> failwith "unexpected type"
    )
    |> Seq.toList
    |> List.mapi (fun idx t -> idx,t)
    |> List.partition (fun (idx,_) -> idx % 2 = 0)
    |> fun (g1,g2) ->
        Columns.columns [Columns.Option.Props [Id "options"]] [ wrapColumn g1 ; wrapColumn g2 ]

let githubIssueUri (m : Model) =
    let location = Fable.Import.Browser.window.location
    let config = m.Config
    let names = FSharpType.GetRecordFields (config.GetType()) |> Seq.map (fun x -> x.Name)
    let values = FSharpValue.GetRecordFields config
    let defaultValues = FSharpValue.GetRecordFields FormatConfig.Default
    let options = Seq.zip3 names values defaultValues
               |> Seq.toArray
               |> Seq.map(fun (k,v, defV) -> sprintf (if v<>defV then "| **`%s`** | **`%A`** |" else "| `%s` | `%A` |") k v)
               |> String.concat "\n"
    let title = "Bug report from fantomas-ui"
    let label = "bug"
    let codeTemplate header code =
        sprintf """
#### %s

```fsharp
%s
```
            """ header code
    let (left,right) =
        match m.FSharpEditorState with
        | Loading -> codeTemplate "Code" m.Source, ""
        | FormatError e ->
            codeTemplate "Code" m.Source, codeTemplate "Error" e
        | Loaded code ->
            codeTemplate "Code" m.Source, codeTemplate "Result" code
    let code = left + "" + right
    let body =
        sprintf """
Issue created from [fantomas-ui](%s)

Please describe here fantomas problem you encountered
%s
%s
#### Options

Fantomas %s

| Name | Value |
| ---- | ----- |
%s
        """ location.href left right m.Version options |> System.Uri.EscapeDataString

    let uri = sprintf "https://github.com/fsprojects/fantomas/issues/new?title=%s&labels=%s&body=%s" title label body
    uri
    |> Href

let safeComponents model =
    let fantomasUIlinks =
        span [ ]
           [
             a [ Href "https://gitlab.com/jindraivanek/fantomas-ui" ] [ str "source code" ]
             str ", "
             a [ Href "https://gitlab.com/jindraivanek/fantomas-ui/issues/new" ] [ str "create issue" ]
           ]

    let fantomasLinks model =
        span [ ]
           [
             a [ Href "https://github.com/fsprojects/fantomas" ] [ str "source code" ]
             str ", "
             a [ githubIssueUri model ] [ str "create issue" ]
           ]

    [ p [ ]
        [ strong [] [ str "Fantomas-UI" ]
          str "(this page): "
          fantomasUIlinks ]
      p [ ]
        [ strong [] [ str "Fantomas: " ]
          fantomasLinks model ] ]

let button disabled txt onClick =
    Button.button
        [ Button.IsFullWidth
          Button.Color IsPrimary
          Button.OnClick onClick
          Button.Disabled disabled ]
        [ str txt ]

let smallButton color txt onClick =
    Button.button
        [ 
          Button.Color color
          Button.IsRounded
          Button.Size IsSmall
          Button.OnClick onClick
        ]
        [ str txt ]    

let notify txt =
    Notification.notification [ Notification.Color IsDanger ] [str txt]

let viewNavbar model (dispatch : Msg -> unit) =
        Navbar.navbar [ Navbar.IsFixedTop ]
                [ Navbar.Brand.div [ ]
                    [ yield Navbar.Item.a [ ]
                        [ strong [ ]
                            [ sprintf "Fantomas v%s" model.Version |> str ] ] 
                      yield! (Config.azureFunctions
                              |> List.map (fun x -> if x.Name <> model.BackendName then 
                                                        smallButton IsGrey x.Name (fun ev -> dispatch (ChangeBackend x.Name))
                                                    else smallButton IsSuccess x.Name ignore))
                    ]      

                  Navbar.End.div [ ]
                    [ Navbar.Item.div [ ]
                        [ Button.a [ Button.Props [ Href "https://gitlab.com/jindraivanek/fantomas-ui" ]
                                     Button.Color IsWarning ]
                            [ Icon.icon [ ]
                                [ Fa.i [Fa.Brand.Gitlab] [] ]
                              span [ ]
                                [ str "Gitlab" ] ] ] ] ]

let sourceAndFormatted model dispatch =
    let sourceTooBig = model.Source.Length > Const.sourceSizeLimit
    let sourceIsEmpty = model.Source.Trim().Length = 0
    let btnFormatDisabled = sourceTooBig || sourceIsEmpty || model.IsLoading
    let headers =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
                        [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "input-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "Type or paste F# code" ] ] ] ]
                          Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "formatted-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "F# code formatted with Fantomas" ] ] ] ] ]

    let editors =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
            [
              Column.column [] [
                Editor.editor [ Editor.Language "fsharp"
                                Editor.IsReadOnly false
                                Editor.Value model.Source
                                Editor.OnChange (SetSourceText >> dispatch) ]
                (if sourceTooBig then notify "Source code size is limited to 10 kB." else div [] [])
                ]
              Column.column [] [
                  Control.div [Control.IsLoading model.IsLoading; Control.CustomClass "is-large"] [
                      Editor.editor [ Editor.Language "fsharp"
                                      Editor.IsReadOnly true
                                      Editor.Value model.Formatted ]
                      (if model.IsLoading || not model.IsError then div [] []
                       else Button.a
                                [ Button.IsFullWidth
                                  Button.Color IsPrimary
                                  Button.IsOutlined
                                  Button.Disabled btnFormatDisabled
                                  Button.Props [githubIssueUri model]
                                ] [str "Looks wrong? Create an issue!"])
            ] ] ]

    [ headers ; editors; button btnFormatDisabled (if model.IsLoading then "Formatting..." else "Format") (fun ev -> dispatch (DoFormat model.Config)) ]

let footer model =
    Footer.footer [ ]
                  [ Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] (safeComponents model) ]

let view (model : Model) (dispatch : Msg -> unit) =
    div []
        [ viewNavbar model dispatch
          div [ Class "page-content"]
                  [
                    yield! sourceAndFormatted model dispatch
                    yield Heading.h3 [ Heading.Props [Id "config-title" ] ] [ str "Config" ]
                    yield options (model.Config) dispatch
                    yield footer model
                  ]

        ]

#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
|> Program.withHMR
#endif
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
