module Server
open System.IO
open System.Threading.Tasks

open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2
open Giraffe
open Shared
open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.Extensions.Logging

open Fable.Remoting.Server
open Fable.Remoting.Giraffe

let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us

module Result =
    let attempt f =
        try
            Ok <| f()
        with e -> Error e

module Reflection =
    open FSharp.Reflection
    let mapRecordToType<'t> (x:obj) =
        let values = FSharpValue.GetRecordFields x
        FSharpValue.MakeRecord(typeof<'t>, values) :?> 't

let lck = obj()

let getFantomasVersion () =
    let assembly = typeof<Fantomas.FormatConfig.FormatConfig>.Assembly
    let version = assembly.GetName().Version
    sprintf "%i.%i.%i" version.Major version.Minor version.Build
    |> fun v -> async { return v }

let init() : Task<Model> = task { return Model.Default }

let format (x: string) config = 
    task { 
        return 
            if x.Length > Const.sourceSizeLimit then Error "Source size limit exceeded." else
            lock lck (fun () -> 
                Result.attempt <| fun () -> Fantomas.CodeFormatter.Parse("tmp.fsx", x)
                |> Result.bind (fun ast ->
                    Result.attempt <| fun () -> 
                        Fantomas.CodeFormatter.FormatAST(ast, "tmp.fsx", Some x, Reflection.mapRecordToType<Fantomas.FormatConfig.FormatConfig> config))
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace)
                |> Result.bind (fun code -> 
                    Result.attempt (fun () -> Fantomas.CodeFormatter.Parse("tmp.fsx", code)) 
                    |> Result.map (fun _ -> code) |> Result.mapError (fun _ -> code))) }

let modelApi = {
    init = init >> Async.AwaitTask
    format = fun x config -> format x config |> Async.AwaitTask
    version = getFantomasVersion
}

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue modelApi
    |> Remoting.buildHttpHandler

// code from https://github.com/giraffe-fsharp/Giraffe.AzureFunctions
[<FunctionName "Giraffe">]
let run ([<HttpTrigger (AuthorizationLevel.Anonymous, Route = "{*any}")>] req : HttpRequest, context : ExecutionContext, log : ILogger) =
  let hostingEnvironment = req.HttpContext.GetHostingEnvironment()
  hostingEnvironment.ContentRootPath <- context.FunctionAppDirectory
  let func = Some >> Task.FromResult
  task {
    let! _ = webApp func req.HttpContext
    req.HttpContext.Response.Body.Flush() //workaround https://github.com/giraffe-fsharp/Giraffe.AzureFunctions/issues/1
    } :> Task