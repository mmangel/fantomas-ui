namespace Shared

module Const =
    let sourceSizeLimit = 10 * 1024

type FormatConfig =
    { /// Number of spaces for each indentation
      IndentSpaceNum : int;
      /// The column where we break to new lines
      PageWidth : int;
      PreserveEndOfLine : bool;
      SemicolonAtEndOfLine : bool;
      SpaceBeforeArgument : bool;
      SpaceBeforeColon : bool;
      SpaceAfterComma : bool;
      SpaceAfterSemicolon : bool;
      IndentOnTryWith : bool;
      /// Reordering and deduplicating open statements
      ReorderOpenDeclaration : bool;
      SpaceAroundDelimiter : bool;
      /// Prettyprinting based on ASTs only
      StrictMode : bool }

    static member Default =
        { IndentSpaceNum = 4; PageWidth = 80;
          PreserveEndOfLine = false;
          SemicolonAtEndOfLine = false; SpaceBeforeArgument = true; SpaceBeforeColon = true;
          SpaceAfterComma = true; SpaceAfterSemicolon = true;
          IndentOnTryWith = false; ReorderOpenDeclaration = false;
          SpaceAroundDelimiter = true; StrictMode = false }

type EditorState =
    | Loading
    | Loaded of string
    | FormatError of string
type Model =
    { Source : string
      Config : FormatConfig
      Version : string
      FSharpEditorState : EditorState
      BackendName : string }
    member __.IsLoading =
        __.FSharpEditorState = Loading
    member __.IsError =
        match __.FSharpEditorState with
        | FormatError _ -> true
        | _ -> false
    member __.Formatted =
        match __.FSharpEditorState with
        | Loading -> ""
        | Loaded code -> code
        | FormatError err -> err
    static member Default =
                            { Source = ""
                              Config = FormatConfig.Default
                              Version = ""
                              FSharpEditorState = Loading
                              BackendName = "current" }

module Route =
    /// Defines how routes are generated on server and mapped from client
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName
    
/// A type that specifies the communication protocol between client and server
/// to learn more, read the docs at https://zaid-ajaj.github.io/Fable.Remoting/src/basics.html
type IModelApi =
    {
        init : unit -> Async<Model>
        version : unit -> Async<string>
        format : string -> FormatConfig -> Async<Result<string, string>>
    }
