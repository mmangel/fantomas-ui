module Config

type AzureFunctionsDef = { 
    Name : string
    AzureFunctionsName :string
    AzureFunctionsUrl : string }

let azureFunctions = [
    {  Name = "current"
       AzureFunctionsName = "fantomas"
       AzureFunctionsUrl = "https://fantomas.azurewebsites.net" }
    {  Name = "preview"
       AzureFunctionsName = "fantomas-preview"
       AzureFunctionsUrl = "https://fantomas-preview.azurewebsites.net" }
    ]

let azureFunctionsName = azureFunctions |> Seq.head |> fun x -> x.AzureFunctionsName
let azureFunctionsUrl = azureFunctions |> Seq.head |> fun x -> x.AzureFunctionsName

let getBackendUrl name = 
    azureFunctions |> Seq.tryFind (fun c -> c.Name = name) |> Option.map (fun c -> c.AzureFunctionsUrl)
    |> Option.defaultValue azureFunctionsUrl