Code for Fantomas online page [https://jindraivanek.gitlab.io/fantomas-ui](https://jindraivanek.gitlab.io/fantomas-ui).

## Pre-requisites

You'll need to install the following pre-requisites in order to build SAFE applications

* The .NET Core SDK 2.1.
* FAKE 5 and Paket installed as a global tool
* The Yarn package manager.
* Node 8.x installed for the front end components.
* Install Azure Functions Core Tool: `npm install -g azure-functions-core-tools`.
* For deploying to Azure install `az`: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest

## Building

Run `fake build -t run` to build and run the app. After a short delay, you'll be presented with application running in your browser. The application will by default run in "development mode", which means it automatically watch your project for changes; whenever you save a file in the client project it will refresh the browser automatically.

## Deploying

Name and URL of Azure Functions App is in `src/Shared/Config.fs` file.

Run `fake build -t bundle` to create Client distribution ready to deploy that targets Azure Functions App.

Run `fake build -t DeployServer` to build and publish Server to Azure Functions. Azure Functions App with name defined in `src/Shared/Config.fs` must exists in your Azure account. You need to be logged in to Azure via `az` CLI beforehand.

### Azure Functions configuration
* In CORS you must allow URL where Client pages will be deployed.

### CI
`deploy.Dockerfile` Dockerfile build Client into `/build/public` dir, and deploy Server into Azure Functions. For logging in it uses [Service Principals](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli?view=azure-cli-latest). Credentials for Service Principal account need to be provided in `AZ_USER`, `AZ_PASS`, `AZ_TENANT` build args. You can use this dockerfile locally with this command:

```
docker build -f deploy.Dockerfile --build-arg AZ_PASS=<password> --build-arg AZ_USER=<username> --build-arg AZ_TENANT=<tenant> .
```

### Summary of manual steps
Steps needed to deploy this on your Azure using GitLab CI:
* Create new Azure Functions App, edit `src/Shared/Config.fs` with its name and URL
* In FunctionApp CORS rules, add new entry with `https://<gitlab_username>.gitlab.io`
* With `az` tool create new Service Principal with `az ad sp create-for-rbac --name ServicePrincipalName`, save credentials into GitLab CI variables with names `SECRET_AZ_USER`, `SECRET_AZ_PASS`, `SECRET_AZ_TENANT`.

